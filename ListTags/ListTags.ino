/**************************************************************************/
/*!
    @file     ListTags.ino
    @author   Adafruit Industries
	@license  BSD (see license.txt)

    This example will wait for any ISO14443A card or tag, and
    depending on the size of the UID will attempt to read from it.

    If the card has a 4-byte UID it is probably a Mifare
    Classic card, and the following steps are taken:

    Reads the 4 byte (32 bit) ID of a MiFare Classic card.
    Since the classic cards have only 32 bit identifiers you can stick
	them in a single variable and use that to compare card ID's as a
	number. This doesn't work for ultralight cards that have longer 7
	byte IDs!

    Note that you need the baud rate to be 115200 because we need to
	print out the data and read from the card at the same time!

This is an example sketch for the Adafruit PN532 NFC/RFID breakout boards
This library works with the Adafruit NFC breakout
  ----> https://www.adafruit.com/products/364

Check out the links above for our tutorials and wiring diagrams
These chips use SPI to communicate, 4 required to interface

Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!
*/
/**************************************************************************/
#undef DEBUG

#include <SerialCommand.h>
#include <EEPROM.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>
#include <LinkedList.h>

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (6)  // DEFAULT 2
#define PN532_RESET (3)  // Not connected by default on the NFC Shield

#define REMOVE_TIME_THRESH 100 // The threshold in milliseconds for human detection of delay.

// Or use this line for a breakout or shield with an I2C connection:
Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

SerialCommand sCmd(&Serial);
int board_id;

class Tag {
  public:
    uint32_t id;
    long startTime;
    long lastRead;
};

// Variables to track tags
LinkedList<Tag*> tags = LinkedList<Tag*>();

void setup() {
  Serial.begin(115200);
  delay(1000);
  
  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    #ifdef DEBUG
    Serial.print("Didn't find PN53x board");
    #endif
    while (1); // halt
  }
  #ifdef DEBUG
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata >> 24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata >> 16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata >> 8) & 0xFF, DEC);
  #endif

  // configure board to read RFID tags
  nfc.SAMConfig();

  #ifdef DEBUG
  Serial.println("Waiting for an ISO14443A Card ...");
  #endif

  sCmd.addCommand("ID",     processID);  // Returns the ID number of the board
  sCmd.setDefaultHandler(unrecognized);      // Handler for command that isn't matched  (says "What?")
  board_id = EEPROM.read(board_id);
}


void loop() {
  sCmd.readSerial();     // We don't do much, just process serial commands

  for( int i=0; i<tags.size(); i++){
    Tag* tag = tags.get(i);
    if ( millis()-tag->lastRead > REMOVE_TIME_THRESH ){ //Save some work later by removing old tags
      Serial.print("R");
      Serial.println(tag->id);
      tags.remove(i);
    }
  }
  
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 50);

  if (success) {

    uint32_t id = uidToCardID(uid);

    // See if the tag we've found is already registered
    Tag* foundTag = NULL;
    for( int i=0; i<tags.size(); i++){
      Tag* tag = tags.get(i);
      if( id == tag->id ){
        foundTag = tag;
        break;
      } else if ( millis()-tag->lastRead > REMOVE_TIME_THRESH ){ //Save some work later by removing old tags
        Serial.print("R");
        Serial.println(tag->id);
        tags.remove(i);
      }
    }

    // Initialize a new tag
    if(!foundTag){
      Tag* newTag = new Tag();
      newTag->id = id;
      newTag->startTime = millis();
      tags.add(newTag);
      foundTag = newTag;
      Serial.print("A");
      Serial.println(id);
    }

    // Reset last read time
    foundTag->lastRead = millis();
  }

  delay(1);
}

uint32_t uidToCardID(uint8_t *uid){
  uint32_t cardid = uid[0];
  cardid <<= 8;
  cardid |= uid[1];
  cardid <<= 8;
  cardid |= uid[2];
  cardid <<= 8;
  cardid |= uid[3];
  return cardid;
}

void processID(){
  Serial.println(board_id);
}

// This gets set as the default handler, and gets called when no other command matches.
void unrecognized(const char *command) {
}
