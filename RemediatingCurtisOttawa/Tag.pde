import processing.video.*;

class Tag {
  long id;
  boolean[] present;
  PShape tagShape;
  color fill_color;
  Movie movie;
  
  Tag(PApplet parent, long i, String movieFile){
    id = i;
    present = new boolean[4];
    
    movie = new Movie(parent, movieFile);
  }
  
  void update(){
    if( isPresent() ){
      movie.loop();
    } else {
      movie.pause();
    }
  }
  
  boolean isPresent(){
    // Is the tag being read at any of the readers
    for( boolean p : present ){
      if( p ){
        return true;
      }
    }
    return false;
  }
}
