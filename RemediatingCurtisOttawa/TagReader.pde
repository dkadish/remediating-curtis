import processing.video.*;

class TagReader {

  Serial port;  // Create object from Serial class
  int quadrant;
  Tag current;
  
  int x,y;

  TagReader(Serial p, int quad, int x_coord, int y_coord) {
    port = p;
    quadrant = quad;
    x = x_coord;
    y = y_coord;
  }

  HashMap<Long, Tag> update(HashMap<Long, Tag> tags) {
    if ( port.available() > 0) {  // If data is available,
      String val = port.readStringUntil('\n');         // read it and store it in val

      try {
        long id = Long.parseLong(val.substring(1).trim());
        Tag tag;
        if ( tags.containsKey(id) ) {
          tag = tags.get(id);

          if ( val.charAt(0) == 'A' ) { // Found a new tag
            tag.present[quadrant] = true;
          } else if ( val.charAt(0) == 'R' ) { // Tag removed
            tag.present[quadrant] = false;
          }
        }
      } catch(NullPointerException e) {
        print("NullPointerException caught");
        e.printStackTrace();
      }
    }

    return tags;
  }
  
  Tag currentTag(HashMap<Long, Tag> tags) {
    for ( Tag t : tags.values () ) {
      if ( t.present[quadrant] ) {
        return t;
      }
    }
  
    return null;
  }
}

