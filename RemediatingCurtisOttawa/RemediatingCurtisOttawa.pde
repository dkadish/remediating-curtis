/** Stephen Foster's Remediating Curtis
 *  
 *  @author David Kadish
 *  
 *  TODO:
 *  Fade sound and video.
 *  Map video to different spots
 */

import processing.video.*;
import processing.serial.*;

import java.util.Map;

final int n_screens = 4;
final int n_movies = 4;

final String[] files = {
  "curtis_comp_loop1.mov",
  "curtis_comp_loop2.mov",
  "hollywood.mov",
  "new_hollywood2.mov" 
};
final long[] tag_ids = {
  157399776L,
  2035137248L,
  965261280L,
  2574559200L
};
final int[] readers = {
  3, 2, 1, 0
};

// Note the HashMap's "key" is a String and "value" is an Integer
HashMap<Long, Tag> tags = new HashMap<Long, Tag>();
TagReader[] quadrants;

ArrayList<Serial> ports;
Tag initTag = null;
  
long currentMappingID = -1;

int[][] q_coords;

void setup() {
  size(displayWidth, displayHeight, P2D);
  noCursor();
  
  q_coords = new int[][]{{0, displayHeight/3},
              {displayWidth/4, displayHeight/3},
              {displayWidth/2, displayHeight/3},
              {3*displayWidth/4, displayHeight/3}};
  
  // Quadrants
  quadrants = new TagReader[4];

  // Serial connections
  ports = new ArrayList<Serial>();
  
  // Load active ports into the ports list
  for(String p : Serial.list()){
    try{
      ports.add(new Serial(this, p, 115200));
    } catch(RuntimeException e){
      // Error opening the serial port
    }
  }
  
  quadrantMapping();
  
  for( int i = 0; i < n_movies; i++ ){
    Tag t = new Tag(this, tag_ids[i], files[i]);
    tags.put(tag_ids[i], t);
  }
  
  println("Done Setup");
}

void draw() {
  
  background(0);  // Set background to white
      
  for(TagReader tr : quadrants){
    if( tr != null ){
      tags = tr.update(tags);
      
      Tag current = tr.currentTag(tags);
    
      if (current != null) {
        image(current.movie, tr.x, tr.y, displayWidth/4, displayHeight/4);
      }
    }  
  }
  
  for ( Tag t : tags.values() ) {
    t.update();
  }
}

void stop(){
  for ( Tag t : tags.values() ) {
    t.movie.stop();
  }
}

// Called every time a new frame is available to read
void movieEvent(Movie m) {
  m.read();
}

/*void keyPressed() {
  if (key >= '0' && key <= '3') { // Set the quadrant if the key is a number
    int k = key - '0';
    println("Waiting to map Quadrant " + k );
    quadrantMapping(k);
  } else if (key == 't') {
    movieMapping();
  }
}*/


int readArduinoIdFromPort(Serial p){
  if ( p.available() > 0) {  // If data is available,
  
    String val = p.readStringUntil('\n');         // read it and store it in val
    
    try {
      int id = Integer.parseInt(val.trim());
      return id;
    } catch(NullPointerException e){
      print("NullPointerException caught");
      e.printStackTrace();
      return -1;
    }
  }
  
  return -1;
}

long readTagIdFromPort(Serial p){
  if ( p.available() > 0) {  // If data is available,
  
    String val = p.readStringUntil('\n');         // read it and store it in val
    
    try {
      long id = Long.parseLong(val.substring(1).trim());
      return id;
    } catch(NullPointerException e){
      print("NullPointerException caught");
      e.printStackTrace();
      return -1;
    }
  }
  
  return -1;
}

void quadrantMapping(){
  for( Serial p : ports ){
    p.write("ID\n");
    delay(100);
    int id = readArduinoIdFromPort(p);
    if( id != -1 ){
      for( int i = 0; i < n_screens; i++ ){
        if( readers[i] == id ){
          quadrants[i] = new TagReader(p, i, q_coords[i][0], q_coords[i][1]);
          break;
        }
      }
    }
  }
}
